import { app, BrowserWindow, dialog } from 'electron'
require('hazardous')
const path = require('path')

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = path.join(__dirname, '/static').replace(/\\/g, '\\\\')
}
global.__extenal = process.env.NODE_ENV !== 'development'
  ? path.join(__dirname, '../../dist/electron/extenal/js').replace(/\\/g, '\\\\')
  : path.resolve(__dirname, 'static/js')
console.log(global.__extenal)
let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

function createWindow() {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 563,
    useContentSize: true,
    width: 1000
  })
  mainWindow.loadURL(winURL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') {
    const { net } = require('electron')
    const request = net.request('http://35.197.138.75:8080/client_config_follow.json')
    request.on('response', (response) => {
      response.on('data', (chunk) => {
        const resJon = JSON.parse(chunk.toString())
        if (resJon.version !== app.getVersion()) {
          const dialogOpts = {
            type: 'info',
            buttons: ['Tôi hiểu'],
            title: 'Cập nhật',
            message: 'Cập nhật phiên bản mới',
            detail: 'Xin hãy vui lòng download phiên bản mới tại trang chủ của "http://shopeelike.com" rất xin lỗi vì sự bất tiện này.',
            icon: global.__static + '/logo.png'
          }
          dialog.showMessageBox(dialogOpts, (response) => {
            if (response === 0) {
              app.quit()
            }
          })
        }
      })
    })
    request.end()
  }
})

process.on('uncaughtException', () => {
  app.quit()
})

