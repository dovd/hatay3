/* eslint-disable */
window.addEventListener('load', () => {
  const {ipcRenderer} = require('electron')
  ipcRenderer.on('listen-action', (event, arg) => {
   if(arg.action == 'search_item'){
     searchItem()
   }
  })
  window.$ = window.jQuery = require('jquery')
  window.axios = require('axios')
  require('jquery.cookie')
  var SHOPEE = {
    INFO_SHOP: 'https://shopee.vn/api/v1/shops/',
  }
  function getSPC_CDS() {
    return '&SPC_CDS=' + window.$.cookie('SPC_CDS')
  }
  searchItem()
  function searchItem() {
      if (window.location.href.indexOf('search') === -1) {
        return;
      }
      let url = window.location.href;
      const arr = url.split('?');
      let api = 'search_items'
      if(arr[1].endsWith('search_user')){
          api = 'search_user'
      }
      var params = new URLSearchParams(window.location.search)
      let newest = 0
      if(params.has('sortBy')){
        params.set('by' , params.get('sortBy'));
        params.delete('sortBy')
      }
      if(params.has('facet')){
        params.set('categoryids' , params.get('facet'));
        params.delete('facet')
      }
      if(params.has('page')){
       newest = params.get('page') * 50;
       params.delete('page')
      }
      params.set('newest' , newest)
      params.set('limit' , 50)
      params.set('order' , 'desc')
      params.set('page_type' , 'search')
      url = 'https://shopee.vn/api/v2/'+api+'/?'+params.toString()
      $.get(url)
      .done(data => {
        let shopids = [];
        for (var i = 0; i < data.items.length; i++) {
          if(!shopids.includes(data.items[i].shopid)){
            shopids.push(data.items[i].shopid)
          }
        }
        if(data.items.length == 0){
          ipcRenderer.send('listen-action-reply', {action: 'save_shops' , isEnd: true})
        }else{
          submitForm(SHOPEE.INFO_SHOP , {shop_ids: shopids}).then((response) => {
            console.log(response.data)
            ipcRenderer.send('listen-action-reply', {action: 'save_shops' , isEnd: false, data: response.data})
          })
        }
      }).fail((err)=>{
          console.log(err.response)
      })
  }
  function submitForm(url , data) {
      return axios.post(url,data,{
          headers: {
              'Cache-Control': 'no-cache' ,
              'accept': 'application/json, text/javascript, */*; q=0.01',
              'x-api-source': 'pc',
              'x-csrftoken': $.cookie('csrftoken'),
              'x-requested-with':'XMLHttpRequest'
          }
      })
  }
})
/* eslint-enable */
