/* eslint-disable */
window.addEventListener('load', () => {
  window.$ = window.jQuery = require('jquery')
  window.axios = require('axios')
  require('jquery.cookie')
  const {ipcRenderer} = require('electron')
  var listLi = $.find("ul.follower-list li");
  var followIndex = 20;
  var SHOPEE = {
    FOLOW_SHOP: 'https://shopee.vn/api/v0/buyer/follow/shop/',
    CHECK_LOGIN: 'https://banhang.shopee.vn/api/v1/login/?SPC_CDS_VER=2'
  }
  function getSPC_CDS() {
    return '&SPC_CDS=' + window.$.cookie('SPC_CDS')
  }
  ipcRenderer.on('listen-action', (event, arg) => {
    if(arg.action == 'login'){
      checkLogin()
    }else if(arg.action == 'follow'){
      if(arg.index > listLi.length){
        ipcRenderer.send('listen-action-reply', {action: 'follow' , data: {isEnd: true , isFollow: false}})
        ipcRenderer.send('listen-action-reply', {action: 'follow-log' , data: 'Đã hết người theo dõi đổi shop mới'})
        return;
      }
      if(followIndex > 0 && (arg.index >= 10 || (arg.index % 10 == 0)) ){
          $.get('https://shopee.vn/shop/'+arg.shopid+'/followers/?offset='+followIndex+'&limit=20&offset_of_offset=0&_='+(+ new Date())+'').done(data => {
              if(data.no_more != undefined && data.no_more){
                followIndex = 0
              }else{
                 followIndex += 20
                 $('ul.follower-list').append(data)
              }
              follow($(listLi[arg.index-1]).attr('data-follower-shop-id') , arg.index-1)
            }).fail((err)=>{
            }) 
      }else{
          follow($(listLi[arg.index-1]).attr('data-follower-shop-id') , arg.index-1);
      }
    } else if(arg.action == 'reset'){
    }
  })
  function checkLogin() {
      $.get(SHOPEE.CHECK_LOGIN + getSPC_CDS())
      .done(data => {
          if(data.id) {
            ipcRenderer.send('listen-action-reply', {action: 'login' , data: {isLogin: true, shop: data}})
          }
      }).fail((err)=>{
          ipcRenderer.send('listen-action-reply', {action: 'login' , data: {isLogin: false}})
      })
  }
  function follow(shopId , index) {
      ipcRenderer.send('listen-action-reply', {action: 'follow-log' , data: ' Bắt đầu theo dõi ' + $('ul.follower-list li[data-follower-shop-id="'+shopId+'"] a').attr('username') })
      if((index + 10) <= listLi.length ){
        scrollToElement($('ul.follower-list li[data-follower-shop-id="'+shopId+'"]'));
      }else{
      }
      if($('ul.follower-list li[data-follower-shop-id="'+shopId+'"] .btn-remove').length == 1 || $('ul.follower-list li[data-follower-shop-id="'+shopId+'"] .btn-follow').hasClass('active')){
        if($('ul.follower-list li[data-follower-shop-id="'+shopId+'"] .btn-remove').length == 1){
          ipcRenderer.send('listen-action-reply', {action: 'follow-log' , data: 'Người theo dõi đã bị khóa tài khoản'})
        }else if($('ul.follower-list li[data-follower-shop-id="'+shopId+'"] .btn-follow').hasClass('active')){
          ipcRenderer.send('listen-action-reply', {action: 'follow-log' , data: $('ul.follower-list li[data-follower-shop-id="'+shopId+'"] a').attr('username') +' đã được theo dõi'})
        }
        ipcRenderer.send('listen-action-reply', {action: 'follow' , data: {isEnd: false , isFollow: true}})
        return;
      }
      submitForm(SHOPEE.FOLOW_SHOP+shopId+'/', {}).then(() => {
          ipcRenderer.send('listen-action-reply', {action: 'follow-log' , data: ' Theo dõi thành công ' + $('ul.follower-list li[data-follower-shop-id="'+shopId+'"] a').attr('username') })
          $('ul.follower-list li[data-follower-shop-id="'+shopId+'"] .btn-follow').text('Đang theo dõi').addClass('active');
      })
      listLi = $.find("ul.follower-list li");
  }
  function submitForm(url , data) {
      return axios.post(url,data,{
          headers: {
              'Cache-Control': 'no-cache' ,
              'accept': 'application/json, text/javascript, */*; q=0.01',
              'x-api-source': 'pc',
              'x-csrftoken': $.cookie('csrftoken'),
              'x-requested-with':'XMLHttpRequest'
          }
      })
  }
  function scrollToElement(element) {
      $('html, body').animate({
          scrollTop:$(element).length ? $(element).offset().top - 44 : 0 
      }, 1000);
  }
})
/* eslint-enable */
