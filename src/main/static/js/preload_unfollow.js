/* eslint-disable */
window.addEventListener('load', () => {
  window.$ = window.jQuery = require('jquery')
  window.axios = require('axios')
  require('jquery.cookie')
  const {ipcRenderer} = require('electron')
  var followIndex = 20
  var SHOPEE = {
    UNFOLOW_SHOP: 'https://shopee.vn/api/v0/buyer/unfollow/shop/',
    CHECK_LOGIN: 'https://banhang.shopee.vn/api/v1/login/?SPC_CDS_VER=2'
  }
  var listLi = $.find("ul.follower-list li")
  function getSPC_CDS() {
    return '&SPC_CDS=' + window.$.cookie('SPC_CDS')
  }
  ipcRenderer.on('listen-action', (event, arg) => {
    if(arg.action == 'login'){
      checkLogin()
    }else if(arg.action == 'reset'){
    }else if(arg.action == 'unfollow'){
        if(arg.index > listLi.length){
          ipcRenderer.send('listen-action-reply', {action: 'unfollow' , data: {isEnd: true , isFollow: false}})
          ipcRenderer.send('listen-action-reply', {action: 'unfollow-log' , data: 'Đã hết người bỏ theo dõi'})
          return;
        }
        if(followIndex > 0 && (arg.index >= 10 || (arg.index % 10 == 0)) ){
            $.get('https://shopee.vn/shop/'+arg.shopid+'/following/?offset='+followIndex+'&limit=20&offset_of_offset=0&_='+(+ new Date())+'').done(data => {
                if(data.no_more != undefined && data.no_more){
                  followIndex = 0
                }else{
                   followIndex += 20
                   $('ul.follower-list').append(data)
                }
                unfollow($(listLi[arg.index-1]).attr('data-follower-shop-id') , arg.index-1)
              }).fail((err)=>{
              })
        }else{
            unfollow($(listLi[arg.index-1]).attr('data-follower-shop-id') , arg.index-1)
        }
      }
  })
  function checkLogin() {
      $.get(SHOPEE.CHECK_LOGIN + getSPC_CDS())
      .done(data => {
          if(data.id) {
            ipcRenderer.send('listen-action-reply', {action: 'login' , data: {isLogin: true, shop: data}})
          }
      }).fail((err)=>{
          ipcRenderer.send('listen-action-reply', {action: 'login' , data: {isLogin: false}})
      })
  }
  function unfollow(shopId , index) {
      ipcRenderer.send('listen-action-reply', {action: 'unfollow-log' , data: ' Bắt đầu bỏ theo dõi ' + $('ul.follower-list li[data-follower-shop-id="'+shopId+'"] a').attr('username') })
      if((index + 10) <= listLi.length ){
        scrollToElement($('ul.follower-list li[data-follower-shop-id="'+shopId+'"]'))
      }
      if($('ul.follower-list li[data-follower-shop-id="'+shopId+'"] .btn-remove').length == 1 || !$('ul.follower-list li[data-follower-shop-id="'+shopId+'"] .btn-follow').hasClass('active')){
        if($('ul.follower-list li[data-follower-shop-id="'+shopId+'"] .btn-remove').length == 1){
          ipcRenderer.send('listen-action-reply', {action: 'unfollow-log' , data: 'Người bỏ theo dõi đã bị khóa tài khoản'})
        }else if(!$('ul.follower-list li[data-follower-shop-id="'+shopId+'"] .btn-follow').hasClass('active')){
          ipcRenderer.send('listen-action-reply', {action: 'unfollow-log' , data: $('ul.follower-list li[data-follower-shop-id="'+shopId+'"] a').attr('username') +' đã được bỏ theo dõi'})
        }
        ipcRenderer.send('listen-action-reply', {action: 'unfollow' , data: {isEnd: false , isUnFollow: true}})
        return;
      }
      submitForm(SHOPEE.UNFOLOW_SHOP+shopId+'/' , {}).then(() => {
      ipcRenderer.send('listen-action-reply', {action: 'follow-log' , data: ' Bỏ theo dõi thành công ' + $('ul.follower-list li[data-follower-shop-id="'+shopId+'"] a').attr('username') })
          $('ul.follower-list li[data-follower-shop-id="'+shopId+'"] .btn-follow').html('+ <span class="L14">Theo dõi</span>').removeClass('active');
      })
      listLi = $.find("ul.follower-list li");
  }
  function submitForm(url , data) {
      return axios.post(url,data,{
          headers: {
              'Cache-Control': 'no-cache' ,
              'accept': 'application/json, text/javascript, */*; q=0.01',
              'x-api-source': 'pc',
              'x-csrftoken': $.cookie('csrftoken'),
              'x-requested-with':'XMLHttpRequest'
          }
      })
  }
  function scrollToElement(element) {
      $('html, body').animate({
          scrollTop:$(element).length ? $(element).offset().top - 44 : 0 
      }, 1000);
  }
})
/* eslint-enable */
