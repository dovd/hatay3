import { generateUUID } from '@/utils/uid'
import { lsConstant } from '@/constant/index'
const SecureLS = require('secure-ls')
const ls = new SecureLS()
const window_ = {
  state: {
    list: []
  },
  mutations: {
    SET_WINDOW_: (state, window_) => {
      state.list.push(window_)
      ls.set(lsConstant.window.key, state.list)
    },
    INIT_DATA: (state) => {
      state.list = ls.get(lsConstant.window.key)
      if (state.list.length === 0) {
        state.list = []
      }
    },
    GET_WINDOW_: (state, window_) => {
      if (state.list.length === 0) {
        const SecureLS = require('secure-ls')
        const ls = new SecureLS()
        state.list = ls.get(lsConstant.window.key)
      }
      for (var i = 0; i < state.list.length; i++) {
        if (window_.uid === state.list[i].uid) {
          return window_
        }
      }
    }
  },

  actions: {
    createWindow({ commit }) {
      return new Promise((resolve, reject) => {
        const { BrowserView } = require('electron').remote
        const uid = generateUUID()
        const view = new BrowserView(
          {
            webPreferences: {
              nodeIntegration: false,
              allowRunningInsecureContent: true,
              allowDisplayingInsecureContent: true,
              webSecurity: false,
              partition: 'persist:' + this.uid
            }
          })
        require('electron').remote.getCurrentWindow().setBrowserView(view)
        const window_ = {
          uid: uid,
          view: view
        }
        commit('SET_WINDOW_', window_)
        resolve(window_)
      })
    },
    openWindow({ commit }, window_) {
      return new Promise((resolve, reject) => {
        const { BrowserView } = require('electron').remote
        const uid = window_.uid
        const view = new BrowserView(
          {
            webPreferences: {
              nodeIntegration: false,
              allowRunningInsecureContent: true,
              allowDisplayingInsecureContent: true,
              webSecurity: false,
              partition: 'persist:' + uid
            }
          })
        require('electron').remote.getCurrentWindow().setBrowserView(view)
        window_.view = view
        resolve(window_)
      })
    },
    detroyWindow({ commit }, window_) {
      return new Promise((resolve, reject) => {
        require('electron').remote.getCurrentWindow().setBrowserView(null)
        if (window_.view !== null) {
          window_.view.destroy()
        }
        resolve()
      })
    },
    initWindowData({ commit }) {
      return new Promise((resolve, reject) => {
        commit('INIT_DATA')
        resolve()
      })
    }
  }
}

export default window_
