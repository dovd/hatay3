import { Status } from '@/constant/index'
import { updateShop, destroyShop, listShop, updateStatus, countShop, summaryShop, getShopExchage } from '@/api/shop_dexie'
const Shop = {
  state: {
    list: []
  },

  mutations: {
  },

  actions: {
    updateShop({ commit }, shop_) {
      const shop = {
        status: Status.shop.NEW,
        shopid: shop_.shopid,
        name: shop_.name,
        following_count: shop_.following_count,
        follower_count: shop_.follower_count,
        ctime: shop_.ctime,
        item_count: shop_.item_count,
        status_code: Status.shop.NEW.code
      }
      return updateShop(shop)
    },
    updateStatus({ commit }, shop_) {
      return updateStatus(shop_)
    },
    listShop({ commit }, query) {
      return listShop(query)
    },
    countShop({ commit }, query) {
      return countShop(query)
    },
    deleteShop({ commit }, shop_) {
      return destroyShop(shop_)
    },
    summaryShop({ commit }) {
      return summaryShop()
    },
    getShopExchage({ commit }) {
      return getShopExchage()
    }
  }
}

export default Shop
