import Dexie from 'dexie'
var db_name = 'shopee'
var db_version = 1
var db = new Dexie(db_name)

export function initTable() {
  return new Promise((resolve, reject) => {
    db.version(db_version).stores({
      shop: '++id, shopid, status_code, item_count, following_count,follower_count,ctime'
    })
    resolve()
  })
}

export function updateShop(shop_) {
  return new Promise((resolve, reject) => {
    const countExist = db.shop.where('shopid').equals(shop_.shopid).count()
    countExist.then(count => {
      if (count === 0) {
        db.shop.add(shop_).then(() => {
          resolve({ result: { isUpdate: false, data: shop_ }})
        })
      } else {
        resolve({ result: { isUpdate: true, data: null }})
      }
    })
  })
}
export function updateStatus(shop_) {
  return new Promise((resolve, reject) => {
    shop_.status_code = shop_.status.code
    console.log(shop_)
    db.shop.update(shop_.id, shop_)
    resolve({ result: { isUpdate: true, data: shop_ }})
  })
}

export function destroyShop(shop_) {
  return new Promise((resolve, reject) => {
    db.shop.delete(shop_.id)
    resolve()
  })
}

export function listShop(query) {
  return new Promise((resolve, reject) => {
    let shops = db.shop
    const isFillet = query.fillter.value !== null && query.fillter.value !== ''
    const isSort = query.sort.prop
    if (query.sort.prop != null) {
      shops = shops.orderBy(query.sort.prop)
    }
    if (isFillet && !isSort) {
      shops = shops.where('status_code').equals(query.fillter.value)
    } else if (isFillet && isSort) {
      shops = shops.and(x => x.status_code === query.fillter.value)
    }
    if (isSort && query.sort.order === 'descending') {
      shops = shops.reverse()
    }
    shops = shops.limit(query.page * query.limit).offset((query.page - 1) * query.limit)
    resolve({ list: shops })
  })
}

export function getShopExchage() {
  return new Promise((resolve, reject) => {
    const shop = db.shop.where('status_code').equals(1).limit(1).offset(0).toArray()
    resolve({ shop: shop })
  })
}

export function countShop(query) {
  return new Promise((resolve, reject) => {
    let shops = db.shop
    if (query.fillter.value !== null && query.fillter.value !== '' && query.fillter.value !== 0) {
      shops = shops.where('status_code').equals(query.fillter.value)
    }
    resolve({ table: shops })
  })
}

export function summaryShop() {
  return new Promise((resolve, reject) => {
    const shopNew = db.shop.where('status_code').equals(0).count()
    const shopFollow = db.shop.where('status_code').equals(1).count()
    const shopUnFollow = db.shop.where('status_code').equals(2).count()
    const shopFollowed = db.shop.where('status_code').equals(3).count()
    resolve({ shopNew: shopNew, shopFollow: shopFollow, shopUnFollow: shopUnFollow, shopFollowed: shopFollowed })
  })
}
