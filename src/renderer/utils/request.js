import axios from 'axios'
import store from '../store'
import { getToken, setToken, removeToken } from './auth'
import Cookies from 'js-cookie'

const service = axios.create({
  baseURL: process.env.BASE_API,
  timeout: 15000
})

service.interceptors.request.use(config => {
  if (store.getters.token) {
    config.headers['Authorization'] = 'Bearer ' + getToken()
  }
  config.headers['X-localization'] = Cookies.get('language') || 'en'
  return config
}, error => {
  console.log(error) // for debug
  Promise.reject(error)
})
service.interceptors.response.use(
  response => {
    if (typeof response.headers.authorization !== 'undefined') {
      const authorization = response.headers.authorization
      const newtoken = authorization.replace('Bearer ', '')
      setToken(newtoken)
    }
    return response
  },
  error => {
    if (error.response.status !== 422) {
      //      Message({
      //        message: error.message,
      //        type: 'error',
      //        duration: 5 * 1000
      //      })
      //      console.log(error)
      removeToken()
    }
    return Promise.reject(error)
  }
)

export default service
