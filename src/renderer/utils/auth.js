const SecureLS = require('secure-ls')
const TokenKey = 'Admin-Token'

export function getToken() {
  const ls = new SecureLS()
  return ls.get(TokenKey)
}

export function setToken(token) {
  const ls = new SecureLS()
  ls.set(TokenKey, token)
  return token
}

export function removeToken() {
  const ls = new SecureLS()
  ls.set(TokenKey, null)
  return true
}
