import moment from 'moment'

export function formatUnix(unix, format) {
  return moment.unix(unix).format(format)
}

