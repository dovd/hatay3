export const Shopee = Object.freeze({
  url: {
    HOME: 'https://shopee.vn/',
    LOGIN: 'https://banhang.shopee.vn/'
  },
  status: {
    NEW: {
      code: 0,
      value: 'Chưa đăng nhập'
    }
  }
})

export const Status = Object.freeze({
  find: {
    NEW: {
      code: 0,
      value: 'Đang dừng',
      type: 'primary',
      text: 'Tìm shop'
    },
    WAIT: {
      code: 1,
      value: 'Đang tìm shop',
      type: 'warning',
      text: 'Dừng tìm kiếm'
    },
    COMPLETED: {
      code: 2,
      value: 'Đã hoàn thành vui lòng chọn từ khóa khác và tiếp tục',
      type: 'primary',
      text: 'Tìm shop'
    }
  },
  shop: {
    NEW: {
      code: 0,
      value: 'Chưa theo dõi'
    },
    FOLLOW: {
      code: 1,
      value: 'Đang theo dõi'
    },
    UNFOLLOW: {
      code: 2,
      value: 'Đang bỏ theo dõi'
    },
    FOLLOWED: {
      code: 3,
      value: 'Đã theo dõi'
    }
  },
  follow: {
    FOLLOW_BTN_STOP: {
      type: 'primary',
      text: 'Theo dõi'
    },
    UNFOLLOW_BTN_STOP: {
      type: 'primary',
      text: 'Bỏ theo dõi'
    },
    BTN_START: {
      type: 'warning',
      text: 'Dừng'
    },
    LOGIN: {
      code: 0,
      value: 'Check đăng nhập'
    },
    SHOP: {
      code: 1,
      value: 'Check shop'
    },
    FOLLOW: {
      code: 2,
      value: 'Đã chạy'
    },
    STOP: {
      code: 3,
      value: 'Đang dừng'
    },
    LOGINED: {
      code: 5,
      value: 'Đã login'
    },
    LOGIN_FAIL: {
      code: 4,
      value: 'Chưa đăng nhập'
    },
    SHOP_FAIL: {
      code: 6,
      value: 'Chưa có shop vui lòng thêm shop vào danh sách'
    },
    UNFOLLOW: {
      code: 7,
      value: 'Đã chạy'
    }
  }
})

