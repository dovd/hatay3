'use strict'

process.env.BABEL_ENV = 'main'

const path = require('path')
const { dependencies } = require('../package.json')
const webpack = require('webpack')

const BabiliWebpackPlugin = require('babili-webpack-plugin')

let mainConfig = {
  entry: {
    preload_find: path.join(__dirname, '../src/main/static/js/preload_find.js'),
    preload_follow: path.join(__dirname, '../src/main/static/js/preload_find.js'),
    preload_unfollow: path.join(__dirname, '../src/main/static/js/preload_find.js'),
  },
  module: {
      rules: [
        {
          test: /\.(js)$/,
          enforce: 'pre',
          exclude: /node_modules/,
          use: {
            loader: 'eslint-loader',
            options: {
              formatter: require('eslint-friendly-formatter')
            }
          }
        },
        {
          test: /\.js$/,
          use: 'babel-loader',
          exclude: /node_modules/
        },
        {
          test: /\.node$/,
          use: 'node-loader'
        }
      ]
    },

    plugins: [
      new webpack.NoEmitOnErrorsPlugin()
    ],
    resolve: {
      extensions: ['.js', '.json', '.node']
    },
  output: {
    filename: '[name].js',
    libraryTarget: 'commonjs2',
    path: path.join(__dirname, '../dist/electron/extenal/js')
  },
  target: "electron-main"
}


module.exports = mainConfig
